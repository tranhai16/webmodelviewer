import { ModelScene } from "@google/model-viewer/dist/model-viewer";
import { BoxGeometry, DirectionalLight, Mesh, MeshBasicMaterial, Scene, SphereGeometry, Vector3 } from "three";

let previousTime = new Date().getTime();
let object: ModelScene;
let modelViewer = document.querySelector('model-viewer');
const minPos = new Vector3();
const maxPos = new Vector3();
console.log(modelViewer);
modelViewer.addEventListener('load', (ev) => {
  console.log(ev);
})
modelViewer.addEventListener('ar-status', (ev: any) => {
  // console.log(ev);
  if (ev.detail.status === 'session-started') {
    // let modelViewerElement = ev.target as ModelViewerElementBase;
    // console.log(modelViewerElement);
    // console.log(object);

  }
  if (ev.detail.status === 'object-placed') {
    object = ev.target[Object.getOwnPropertySymbols(ev.target).find(e => e.description === 'scene')] as ModelScene;
    let scene = new Scene();
    scene.castShadow = true;
    
    let light = new DirectionalLight(0xffffff, 1);
    light.position.set(0, 10, 0);
    light.castShadow = true;
    scene.add(light);
    minPos.set(object.goalTarget.x - 2, object.goalTarget.y, object.goalTarget.z);
    maxPos.set(object.goalTarget.x + 2, object.goalTarget.y, object.goalTarget.z);
    let object3d = object.target;
    let boxGeometry = new SphereGeometry(1, 32, 32);
    let boxMaterial = new MeshBasicMaterial({color: 0xff0000});
    let box = new Mesh(boxGeometry, boxMaterial);
    box.receiveShadow = true;
    box.castShadow = true;
    scene.add(box);
    object.add(scene);
    console.log(object3d);
  }
});
let speed = 0;
function update(dt: number) {
  if (object) {
    // if(speed === 0) speed = dt;
    // if(object.goalTarget.x >= maxPos.x) speed = -dt;
    // else if(object.goalTarget.x <= minPos.x) speed = dt;
    // object.goalTarget.set(object.goalTarget.x + speed, object.goalTarget.y, object.goalTarget.z + speed);
    object.target.rotateY(dt);
    object.updateMatrixWorld();
  }
}

function animation() {
  let currentTime = new Date().getTime();
  let dt = (currentTime - previousTime) / 1000;
  previousTime = currentTime;
  update(dt);
  requestAnimationFrame(animation);
}
animation();